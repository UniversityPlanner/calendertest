package com.example.clusterbox.calendertestbeta;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public TextView monthTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        monthTitle = (TextView)findViewById(R.id.month_title);

        if (savedInstanceState == null) {
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            CalenderFrag fragment = new CalenderFrag();
            transaction.replace(R.id.FrameContainer, fragment);
            transaction.commit();
        }

    }
}
